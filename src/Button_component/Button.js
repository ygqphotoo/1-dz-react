import React from "react";

class Button extends React.Component{
    constructor(props) {
        super(props);
    }
    render() {
        return(
        <button className={this.props.className} text={this.props.text} title={this.props.title} onClick={this.props.onClick}>button</button>
        )
    }
}

export default Button;