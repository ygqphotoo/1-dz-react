import React from "react";
import "../ModalStyles/ModalSecondStyles.css"

class ModalSecond extends React.Component{
    render()
    {
        return(
            <>
                {this.props.isOpen &&
                    <div className="modal">
                        <div className="modal__content_second" >
                            <button className="closeActive" onClick={this.props.onCancel} >X</button>
                            <h1 className="modal__title_second">{this.props.header}</h1>
                            <p className="modal__text_second">{this.props.text}</p>
                            <div className="btn__position">
                                <button className="super">{this.props.actionsGood}</button>
                                <button className="bad">{this.props.actionsBad}</button>
                            </div>
                        </div>
                    </div>
                }
            </>
        )
    }
}


export default ModalSecond;