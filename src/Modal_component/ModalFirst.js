import React from "react";
import "../ModalStyles/ModalFirstStyle.css"

class ModalFirst extends React.Component{
    render()
    {
        return(
            <>
                {this.props.isOpen &&
                    <div className="modal">
                        <div className="modal_content" style={this.props.backgroundColor} >
                            <button className="closeActive_first" onClick={this.props.onCancel} >X</button>
                            <h1 className="modal__title_first">Как ваше настроение?</h1>
                            <p className="modal__text_first">{this.props.text}</p>
                            <div className="btn__position">
                                <button className="super_first">Отличное</button>
                                <button className="bad_first">Плохое</button>
                            </div>
                        </div>
                    </div>
                }
            </>
        )
    }
}



export default ModalFirst;