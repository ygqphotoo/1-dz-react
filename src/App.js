import React from "react";
import './App.css';
import Button from "./Button_component/Button";
import ModalFirst from "./Modal_component/ModalFirst";
import ModalSecond from "./Modal_component/ModalSecond";

class App extends React.Component{
    constructor(props) {
        super(props)
        this.state = {
            firstOpen: false,
            secondOpen: false,
        };
    }
    handleFirstModalOpen(){
        this.setState((state) => ({
            ...state,
            firstOpen: !state.firstOpen
        }))
    }
    handleSecondModalOpen(){
        this.setState((state) => ({
            ...state,
            secondOpen: !state.secondOpen
        }))
    }
  render(){
    return(
      <div>
        <Button className="propsBtnFirst"
                onClick = {this.handleFirstModalOpen.bind(this)}
        />
        <ModalFirst
            backgroundColor = {{backgroundColor: 'darkred'}}
            isOpen={this.state.firstOpen}
            text = {"Уточните пожалуйста"}
            onCancel={() =>{
            this.handleFirstModalOpen()
            }}
        />
        <Button className="propsBtnSecond"
                onClick = {this.handleSecondModalOpen.bind(this)} />
        <ModalSecond
            isOpen = {this.state.secondOpen}
            text = {"Уточните пожалуйста"}
            header = {"Как ваше здоровье?"}
            onCancel = {() =>{this.handleSecondModalOpen()}}
            actionsGood = {"Отлично"}
            actionsBad = {"Плохо"}
        />
      </div>

    )

  }
}

export default App;
